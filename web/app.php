<?php

@include_once dirname(__FILE__) . '/../app/config/config_dev.php';
require_once dirname(__FILE__) . '/../app/config/config_prod.php';

require_once CORELIB_BASE_DIRECTORY . '/Component/AppKernel.php';

$kernel = new Webninja_AppKernel();

//set_include_path(dirname(__FILE__) . '/../src/SiteName' . PATH_SEPARATOR . get_include_path());
//$siteBundle = new SiteBundle_SiteBundle();

$kernel->handle();