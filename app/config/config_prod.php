<?php

$config = array(
    'HTTP_SERVER'                  => 'http://timshaw.webninjashops.com',
    'HTTPS_SERVER'                 => 'https://timshaw.webninjashops.com',
    'HTTP_COOKIE_DOMAIN'           => 'timshaw.webninjashops.com',
    'HTTPS_COOKIE_DOMAIN'          => 'timshaw.webninjashops.com',

    'DIR_FS_CATALOG'               => '/var/www/sites/timshaw/',

    'DB_SERVER_USERNAME'           => 'timshaw',
    'DB_SERVER_PASSWORD'           => 'u2vs2jig',

    'CMS_BASE_DIRECTORY'           => '/var/www/cms/',
    'CORELIB_BASE_DIRECTORY'       => '/var/www/corelib/',
);

foreach ($config as $key => $value) {
    if (!defined($key)) {
        define($key, $value);
    }
}
